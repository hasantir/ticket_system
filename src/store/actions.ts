import { RootState } from "@/types";

import { ActionContext } from "vuex";
export const actions = {
  setWidth(
    { commit }: ActionContext<RootState, RootState>,
    width: number
  ): void {
    commit("SET_WIDTH", width);
  },
};
