import { RootState } from "@/types";
export const mutations = {
  SET_WIDTH(state: RootState, width: number): void {
    state.screenWidth = width;
  },
};
