import { RootState } from "@/types";

export const state = {
  screenWidth: window.innerWidth,
} as RootState;
