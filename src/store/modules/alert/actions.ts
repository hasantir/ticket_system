import { RootState } from "@/types";
import { AlertState } from "@/types/alert";
import { ActionContext } from "vuex";

export default {
  hide({ commit }: ActionContext<AlertState, RootState>): void {
    commit("HIDE");
  },
  show(
    { commit }: ActionContext<AlertState, RootState>,
    { text, type, duration }: { text: string; type: string; duration: number }
  ): void {
    commit("SHOW", { show: true, text, type: type });
    setTimeout(() => {
      commit("HIDE");
    }, duration * 1000);
  },
};
