import { AlertState } from "@/types/alert";

export default {
  SHOW(state: AlertState, payload: AlertState): void {
    state.show = payload.show;
    state.text = payload.text;
    state.type = payload.type;
  },
  HIDE(state: AlertState): void {
    state.show = false;
  },
};
