import { AlertState } from "@/types/alert";
export default {
  show: false,
  text: "",
  type: "",
} as AlertState;
