import { RootState } from "@/types";
import { Ticket, TicketList, TicketState } from "@/types/ticket";
import { ActionContext } from "vuex";
import {
  getTickets,
  ticketDetail,
  addAnswer,
  createTicket,
  closeTicket,
} from "@/mock/tickets";
export default {
  async list({
    commit,
  }: ActionContext<TicketState, RootState>): Promise<TicketList> {
    const data = await getTickets();
    commit("SET_LIST", data);
    return data;
  },
  async detail(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    { commit }: ActionContext<TicketState, RootState>,
    id: number
  ): Promise<Ticket> {
    return await ticketDetail(id);
  },
  addAnswer(
    { commit }: ActionContext<TicketState, RootState>,
    { id, message }: { id: number; message: string }
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      addAnswer(id, message)
        .then((res) => {
          commit("ADD_ANSWER", res);
          resolve(true);
        })
        .catch(() => {
          reject(false);
        });
    });
  },
  addTicket(
    { commit }: ActionContext<TicketState, RootState>,
    { title, message }: { title: string; message: string }
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      createTicket({ title, message })
        .then((res) => {
          commit("ADD_TICKET", res);
          resolve(true);
        })
        .catch(() => {
          reject(false);
        });
    });
  },
  async closeTicket(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    { commit }: ActionContext<TicketState, RootState>,
    id: number
  ): Promise<Ticket> {
    const data = await closeTicket(id);

    commit("CLOSE_TICKET", id);
    return data;
  },
};
