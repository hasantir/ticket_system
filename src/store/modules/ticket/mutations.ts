import { Answer, Ticket, TicketList, TicketState } from "@/types/ticket";
export default {
  SET_LIST(state: TicketState, data: TicketList): void {
    state.list = data;
  },
  ADD_ANSWER(state: TicketState, data: Answer): void {
    const index = state.list.findIndex((item) => {
      return item.id === Number(data.id);
    });
    if (index !== -1) {
      state.list[index].answers.unshift(data);
    }
  },
  ADD_TICKET(state: TicketState, data: Ticket): void {
    state.list.unshift(data);
  },
  CLOSE_TICKET(state: TicketState, id: number): void {
    const index = state.list.findIndex((item) => {
      return item.id === Number(id);
    });
    if (index !== -1) {
      state.list[index].status = "closed";
    }
  },
};
