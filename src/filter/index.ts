import Vue from "vue";
Vue.filter("formatDate", function (value: number) {
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const date = new Date(value);

  return (
    date.getFullYear() + " " + months[date.getMonth()] + " " + date.getDate()
  );
});
