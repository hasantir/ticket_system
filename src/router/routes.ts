import { RouteConfig } from "vue-router";
const routes: Array<RouteConfig> = [
  {
    path: "",
    name: "dashboard",
    component: () => import("@/layouts/Dashboard.vue"),
    children: [
      {
        path: "/",
        name: "tickets",
        component: () => import("@/views/dashboard/ticket/tickets.vue"),
      },
      {
        path: "/ticket/create",
        name: "new-ticket",
        component: () => import("@/views/dashboard/ticket/newTicket.vue"),
      },
      {
        path: "/ticket/:id",
        name: "ticket-detail",
        component: () => import("@/views/dashboard/ticket/ticketDetail.vue"),
      },
    ],
  },
];

export default routes;
