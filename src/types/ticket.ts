export interface Answer {
  id: number;
  message: string;
}
export type AnswersList = Array<Answer>;
export interface Ticket {
  id: number;
  title: string;
  message: string;
  created_at: number;
  status: "pending" | "closed" | "answered";
  answers: Array<Answer>;
}
export type TicketList = Array<Ticket>;
export interface TicketState {
  list: TicketList;
}

export interface TicketPaylod {
  id?: number;
  title: string;
  message: string;
  created_at?: number;
  status?: "pending" | "closed" | "answered";
  answers?: Array<Answer>;
}
