export interface AlertPrototype {
  show: (text: string) => void;
  error: (text: string) => void;
  success: (text: string) => void;
}
