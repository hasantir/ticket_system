export interface AlertState {
  show: boolean;
  type: string;
  text: string;
}
