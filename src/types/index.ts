import { TicketState } from "./ticket";
export type RootState = {
  screenWidth: number;
  ticket: TicketState;
};
