import store from "@/store";
import Vue from "vue";

const alert = {
  error(text: string, duration = 4) {
    store.dispatch("alert/show", {
      text: text,
      type: "error",
      duration: duration,
    });
  },
  success(text: string, duration = 4) {
    store.dispatch("alert/show", {
      text: text,
      type: "success",
      duration: duration,
    });
  },
};
Vue.prototype.$alert = alert;
