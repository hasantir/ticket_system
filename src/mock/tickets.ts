import { Answer, Ticket, TicketList } from "@/types/ticket";

const data = [
  {
    id: 3,
    title: "Third Ticket Title",
    message: "Third Ticket Message",
    created_at: 1630039197000,
    status: "pending",
    answers: [],
  },
  {
    id: 2,
    title: "Second Ticket Title",
    message: "Second Ticket Message",
    created_at: 1629952797000,
    status: "answered",
    answers: [
      {
        id: 1,
        message: "please call to support",
      },
    ],
  },
  {
    id: 1,
    title: "First Ticket Title",
    message: "First Ticket Message",
    created_at: 1629866397000,
    status: "closed",
    answers: [],
  },
] as TicketList;

export const getTickets = (): Promise<TicketList> => {
  return new Promise((resolve) => {
    resolve(data);
  });
};

export const createTicket = ({
  title,
  message,
}: {
  title: string;
  message: string;
}): Promise<Ticket> => {
  return new Promise((resolve) => {
    const id = Math.floor(Math.random() * 101);
    const new_ticket = {
      title: title,
      message: message,
      id: id,
      created_at: Number(Date.now()),
      answers: [],
      status: "pending",
    } as Ticket;

    //data.unshift(new_ticket);
    resolve(new_ticket);
  });
};

export const ticketDetail = (id: number): Promise<Ticket> => {
  return new Promise((resolve, reject) => {
    const index = data.findIndex((item) => {
      return item.id === Number(id);
    });
    if (index !== -1) resolve(data[index]);
    else reject("ticket not exist!");
  });
};

export const addAnswer = (id: number, message: string): Promise<Answer> => {
  return new Promise((resolve, reject) => {
    const index = data.findIndex((item) => {
      return item.id === Number(id);
    });
    if (index !== -1) {
      const id = Math.floor(Math.random() * 101);
      const answer = {
        id: id,
        message: message,
      };
      data[index].answers.unshift(answer);
      data[index].status = "answered";
      resolve(answer);
    } else reject("problem in add answer");
  });
};

export const closeTicket = (id: number): Promise<Ticket> => {
  return new Promise((resolve, reject) => {
    const index = data.findIndex((item) => {
      return item.id === Number(id);
    });
    if (index !== -1) {
      data[index].status = "closed";
      resolve(data[index]);
    } else reject("ticket not exist!");
  });
};
