import { AlertPrototype } from "@/types/prototypes";
import { Api } from "@/types/api";
import { Db } from "@/types/db";

declare module "vue/types/vue" {
  interface Vue {
    $alert: AlertPrototype;
    $api: Api;
    $db: Db;

    $vm2;
  }
}
